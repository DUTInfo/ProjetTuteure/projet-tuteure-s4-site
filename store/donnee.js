export const state = () => ({
  telemetrie: [],
  donnee: []
})

export const getters = {

}

export const actions = {
  async updateDonnee(context) {
    try {
      const gpssensors = await this.$axios.$get("http://localhost:7000/_/items/GPSsensors")
      context.commit('SET_TELEMETRIE', gpssensors.data)
      const readings = await this.$axios.$get("http://localhost:7000/_/items/Readings")
      context.commit('SET_DONNEE', readings.data)
    } catch (e) {
      throw e
    }
    // setTimeout(context.dispatch('pompiers/updatePompiers'), context.rootState.parametres.intervalleRafraichissement)
  }
}

export const mutations = {
  SET_TELEMETRIE (state, telemetrie) {
    state.telemetrie = telemetrie
  },
  SET_DONNEE (state, donnee) {
    state.donnee = donnee
  }
}
