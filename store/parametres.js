export const state = () => ({
  intervalle: 10000,
  temperatureMaximale: 100,
  pressionMaximale: 0
})

export const getters = {

}

export const actions = {
  setIntervalle({ commit }, intervalle) {
    if (Number(intervalle) >= 0.1) {
      commit('SET_INTERVALLE', Number(intervalle) * 1000)
    }
  },
  setTemperatureMaximale({ commit }, temperatureMaximale) {
    if (Number(temperatureMaximale) >= -273.15) {
      commit('SET_TEMPERATURE_MAXIMALE', Number(temperatureMaximale))
    }
  },
  setPressionMaximale({ commit }, pressionMaximale) {
    commit('SET_PRESSION_MAXIMALE', Number(pressionMaximale))
  },
  resetParametres({ commit }) {
    commit('SET_INTERVALLE', 10000)
    commit('SET_TEMPERATURE_MAXIMALE', 100)
    commit('SET_PRESSION_MAXIMALE', 0)
  }
}

export const mutations = {
  SET_INTERVALLE (state, intervalle) {
    state.intervalle = intervalle
  },
  SET_TEMPERATURE_MAXIMALE (state, temperatureMaximale) {
    state.temperatureMaximale = temperatureMaximale
  },
  SET_PRESSION_MAXIMALE (state, pressionMaximale) {
    state.pressionMaximale = pressionMaximale
  }
}
