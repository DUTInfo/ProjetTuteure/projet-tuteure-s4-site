# projet-tuteure-s4

> Site web pour le suivi des pompiers

## Étapes pour la mise en production

### Installer les dépendances
``` bash
$ npm install
```

### Modifier l'emplacement de l'application sur le serveur web

Par défaut, l'application doit se trouver à la racine du serveur web.

Pour modifier l'emplacement de l'application sur le serveur web, il faut que la valeur de `base` dans le fichier [nuxt.config.js](nuxt.config.js "Consulter le fichier") soit en adéquation avec l'arborescence souhaitée.

Par exemple, si l'on souhaite que l'application soit disponible à `/app/`, il suffit de modifier la valeur de `base` par `/app/` :

``` javascript
module.exports = {
  ...
  router: {
    base: '/app/'
  },
  ...
}
```

### Générer l'application web

Afin que l'application web n'ait besoin que d'un serveur web statique, il faut générer les sources :

``` bash
$ npm run generate
```

### Déploiement de l'application

Pour mettre en ligne l'application, il suffit de copier le contenu du dossier `dist` à la racine du serveur web.

## More on NuxtJS

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
